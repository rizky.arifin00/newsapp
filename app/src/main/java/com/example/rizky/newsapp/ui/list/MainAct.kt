package com.example.rizky.newsapp.ui.list

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.example.rizky.newsapp.R
import com.example.rizky.newsapp.base.BaseAct
import com.example.rizky.newsapp.model.NewsResponse
import com.example.rizky.newsapp.ui.detail.DetailNewsAct
import com.jcodecraeer.xrecyclerview.ProgressStyle
import com.jcodecraeer.xrecyclerview.XRecyclerView
import kotlinx.android.synthetic.main.activity_main.*


class MainAct : BaseAct(), MainView {

    private lateinit var mPresenter: MainPresenter
    private lateinit var mMainAdapter: MainAdapter

    private var page = 1

    private var isSwipeRefresh = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initPresenter()
        initRecyler()
        onAttachView()

        getData()

        swipe_refresh.setOnRefreshListener {
            this.page = 0
            isSwipeRefresh = true
            getData()
        }
    }

    private fun initRecyler() {
        rv_news.layoutManager = LinearLayoutManager(this)
        rv_news.setLoadingMoreProgressStyle(ProgressStyle.BallRotate)
        rv_news.setPullRefreshEnabled(false)
        rv_news.setLimitNumberToCallLoadMore(1)
        rv_news.setFootViewText("Loading ...", "")
        (rv_news.itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false

        mMainAdapter = MainAdapter(arrayListOf(), {
            val intent = Intent(this, DetailNewsAct::class.java)
            intent.putExtra("news", it)
            startActivity(intent)
        })

        rv_news.adapter = mMainAdapter

        rv_news.setLoadingListener(object : XRecyclerView.LoadingListener {
            override fun onRefresh() {

            }

            override fun onLoadMore() {
                isSwipeRefresh = false
                page += 1
                mPresenter.getTopHeadLines("us", page)
            }
        })

    }

    private fun getData() {
        mPresenter.getTopHeadLines("us", page)
    }

    private fun initPresenter() {
        mPresenter = MainPresenter()
    }

    override fun onAttachView() {
        mPresenter.onAttach(this)
    }

    override fun onDetachView() {
        mPresenter.onDetach()
    }

    override fun onDestroy() {
        super.onDestroy()
        onDetachView()
    }

    override fun showLoading() {
        if (isSwipeRefresh)
            swipe_refresh.isRefreshing = true
    }

    override fun dismissLoading() {
        if (isSwipeRefresh)
            swipe_refresh.isRefreshing = false
    }

    override fun onSuccess(list: NewsResponse) {
        if (page != 0) {
            Handler().postDelayed({
                for (data in list.articles) {
                    mMainAdapter.add(data)
                }
                if (rv_news != null) {
                    rv_news.loadMoreComplete()
                    mMainAdapter.notifyDataSetChanged()
                }
            }, 1000)
            if (list.articles.isEmpty()) {
                Handler().postDelayed({
                    if (rv_news != null) {
                        rv_news.setNoMore(true)
                        mMainAdapter.notifyDataSetChanged()
                    }
                }, 1000)
            }
        } else {
            mMainAdapter.clear()
            mMainAdapter.addAll(list.articles)
        }
    }

    override fun onFailed(message: String) {

    }
}
