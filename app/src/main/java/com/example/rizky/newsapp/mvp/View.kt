package com.example.rizky.newsapp.mvp

interface View {
    fun onAttachView()
    fun onDetachView()
}