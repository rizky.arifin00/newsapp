package com.example.rizky.newsapp.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    companion object {
        fun getFormatedDate(strDate: String): String {
            val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            val formatAfter = SimpleDateFormat("dd/MM/yyyy");
            val date = format.parse(strDate)

            return formatAfter.format(date);
        }
    }

}