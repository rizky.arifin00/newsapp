package com.example.rizky.newsapp.ui.list

import com.example.rizky.newsapp.model.NewsResponse
import com.example.rizky.newsapp.mvp.View

interface MainView : View {
    fun showLoading()
    fun dismissLoading()
    fun onSuccess(list : NewsResponse)
    fun onFailed(message : String)
}