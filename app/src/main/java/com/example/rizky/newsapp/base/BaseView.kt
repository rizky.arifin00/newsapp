package com.example.rizky.newsapp.base

interface BaseView {
    fun onShowLoading()
    fun onDismissLoading()
}