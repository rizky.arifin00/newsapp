package com.example.rizky.newsapp.ui.detail

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.example.rizky.newsapp.R
import com.example.rizky.newsapp.base.BaseAct
import com.example.rizky.newsapp.model.Article
import com.example.rizky.newsapp.utils.DateUtil
import kotlinx.android.synthetic.main.activity_detail_news.*

class DetailNewsAct : BaseAct() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_news)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initData(intent.extras.get("news") as Article)
    }

    fun initData(news: Article) {
        lateinit var author: String

        val date = DateUtil.getFormatedDate(news.publishedAt)
        if (news.author == null || news.author.length > 40) {
            author = news.source.name
        } else {
            author = news.author
        }

        if (news.urlToImage == null)
            img_frame.visibility = View.GONE
        else
            Glide.with(this).load(news.urlToImage).into(img_news)

        txt_title_news.text = news.title
        txt_info_news.text = "By $author on $date"
        txt_content.text = news.content
        txt_link.text = news.url
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
