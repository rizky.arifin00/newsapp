package com.example.rizky.newsapp.ui.list

import com.example.rizky.newsapp.model.NewsResponse
import com.example.rizky.newsapp.mvp.Presenter
import com.example.rizky.newsapp.network.ApiClient
import com.example.rizky.newsapp.network.ApiService
import com.example.rizky.newsapp.utils.Network
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class MainPresenter : Presenter<MainView> {

    lateinit var mApiService: ApiService

    var mView : MainView? = null

    override fun onAttach(V: MainView) {
        mView = V
        mApiService = ApiClient.initRetrofit().create(ApiService::class.java)
    }

    override fun onDetach() {
        mView = null
    }

    fun getTopHeadLines(country : String, page : Int){
        mView?.showLoading()
        mApiService.getTopHeadLines(country, Network.API_KEY, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<NewsResponse> {
                    override fun onError(e: Throwable) {
                        mView?.dismissLoading()
                        mView?.onFailed(e.message.toString())
                    }

                    override fun onNext(t: NewsResponse) {
                        mView?.onSuccess(t)
                    }

                    override fun onCompleted() {
                        mView?.dismissLoading()
                    }
                })
    }
}