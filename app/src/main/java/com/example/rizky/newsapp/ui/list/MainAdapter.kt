package com.example.rizky.newsapp.ui.list

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.rizky.newsapp.R
import com.example.rizky.newsapp.model.Article
import com.example.rizky.newsapp.utils.DateUtil
import kotlinx.android.synthetic.main.item_news.view.*

class MainAdapter(private val newsList: MutableList<Article>, private val itemClickListener: (Article) -> Unit)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var mContext: Context

    companion object {
        const val FIRST_NEWS = 0
        const val IMAGE_NEWS = 1
        const val TEXT_NEWS = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context

        val inflater = LayoutInflater.from(mContext)

        return when (viewType) {
            FIRST_NEWS -> ViewHolder(inflater.inflate(R.layout.item_first_news, parent, false))
            TEXT_NEWS -> TextViewHolder(inflater.inflate(R.layout.item_text_news, parent, false))
            else -> ViewHolder(inflater.inflate(R.layout.item_news, parent, false))

        }
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return FIRST_NEWS
        } else if (newsList[position].urlToImage == null) {
            return TEXT_NEWS
        }
        return IMAGE_NEWS
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val news = newsList[position]
        lateinit var author: String

        if (news.author == null || news.author.length > 40) {
            author = news.source.name
        } else {
            author = news.author
        }

        val date = DateUtil.getFormatedDate(news.publishedAt)

        when (getItemViewType(position)) {
            TEXT_NEWS -> {
                val textViewHolder = holder as TextViewHolder

                textViewHolder.txtTitleNews.text = news.title
                textViewHolder.txtInfoNews.text = "By $author , on $date"
                textViewHolder.itemView.setOnClickListener { itemClickListener(news) }
            }
            else -> {
                val viewHolder = holder as ViewHolder

                Glide.with(mContext).load(news.urlToImage).into(viewHolder.imgNews)
                viewHolder.txtTitleNews.text = news.title
                viewHolder.txtInfoNews.text = "By $author on $date"
                viewHolder.itemView.setOnClickListener { itemClickListener(news) }
            }
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imgNews: ImageView = view.img_news
        val txtTitleNews: TextView = view.txt_title_news
        val txtInfoNews: TextView = view.txt_info_news
    }

    inner class TextViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtTitleNews: TextView = view.txt_title_news
        val txtInfoNews: TextView = view.txt_info_news
    }

    fun add(article: Article) {
        newsList.add(article)
        notifyDataSetChanged()
    }

    fun addAll(newsList: MutableList<Article>) {
        this.newsList.addAll(newsList)
        notifyDataSetChanged()
    }

    fun clear() {
        newsList.clear()
        notifyDataSetChanged()
    }
}