package com.example.rizky.newsapp.mvp

interface Presenter<V> {
    fun onAttach (view : V)
    fun onDetach ()
}