package com.example.rizky.newsapp.network

import com.example.rizky.newsapp.model.NewsResponse
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable
import java.util.*

interface ApiService {

    @GET("top-headlines")
    fun getTopHeadLines(@Query("country") country : String,
                        @Query("apiKey") apiKey : String,
                        @Query("page") page : Int) : Observable<NewsResponse>

}